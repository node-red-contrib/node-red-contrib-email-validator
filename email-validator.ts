import {Node, NodeStatus, Red} from "node-red";
import {Message} from "./libs/@types/node-red";
import {NodeRedEvent, NodeRedStatus, NodeRedStatusFill, NodeRedStatusShape} from "./libs/node-red";

module.exports = function (RED: Red) {

    let validate: any = require("validate.js");

    function EmailValidatorNode(config: any): void {

        RED.nodes.createNode(this, config);

        let nodeStatus:NodeStatus;
        let nodeStatusClear:NodeStatus = NodeRedStatus.create();
        let nodeStatusSuccess:NodeStatus = NodeRedStatus.create(NodeRedStatusFill.Green, NodeRedStatusShape.Dot, 'Success');
        let nodeStatusError:NodeStatus = NodeRedStatus.create(NodeRedStatusFill.Red, NodeRedStatusShape.Dot, 'Error');

        let node: Node = this;
        node.on(NodeRedEvent.Input, function (msg: Message) {

            let constraints = {
                value: {
                    email: true
                }
            };

            let validationError: any = validate({value: msg.payload}, constraints);
            if (validationError) {
                msg.payload = validationError;
            }

            nodeStatus = validationError ? nodeStatusError : nodeStatusSuccess;
            node.status(nodeStatus);
            setTimeout(() => {
                node.status(nodeStatusClear);
            }, 2000);

            let msgs:Array<Message> = validationError ? [null, msg] : [msg, null];
            node.send(msgs);
        });
    }

    RED.nodes.registerType("email-validator", EmailValidatorNode);
}
