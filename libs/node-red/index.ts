import {NodeStatus} from "node-red";

export enum NodeRedEvent {
    Input = 'input'
}

export enum NodeRedStatusFill {
    Red = 'red',
    Green = 'green',
    Yellow = 'yellow',
    Blue = 'blue',
    Grey = 'grey'
}

export enum NodeRedStatusShape {
    Ring = 'ring',
    Dot = 'dot'
}

export class NodeRedStatus {
    public static create(
        fill?: NodeRedStatusFill,
        shape?: NodeRedStatusShape,
        text?: string
    ): NodeStatus {
        return {
            fill: fill,
            shape: shape,
            text: text
        }
    }
}