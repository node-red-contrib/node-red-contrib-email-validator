# node-red-contrib-email-validator
A simple Email Validator for Node-RED based on ValidateJS.

> **Disclaimer:**
This is mostly an exercise to do a simple exploration on creating a Node-RED contribution, the very simple nature of this project made it quite ideal for that.
>

## Reference

### Input
msg.payload : [email]

### Output
#### Success
Passes the input Email unaltered.
#### Error
Passes the unaltered ValidateJS Errors: https://validatejs.org/#validators-email

## Example

![Example-1](https://gitlab.com/node-red-contrib/node-red-contrib-email-validator/-/raw/master/examples/example-1.png)

```json
[
    {
        "id": "5c425f7e.794ee",
        "type": "tab",
        "label": "Email Validator - Example Flow",
        "disabled": false,
        "info": ""
    },
    {
        "id": "9a6d8a8a.3facf8",
        "type": "inject",
        "z": "5c425f7e.794ee",
        "name": "Error",
        "topic": "",
        "payload": "",
        "payloadType": "date",
        "repeat": "",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "x": 470,
        "y": 140,
        "wires": [
            [
                "a163646e.aaf7d8"
            ]
        ]
    },
    {
        "id": "a163646e.aaf7d8",
        "type": "email-validator",
        "z": "5c425f7e.794ee",
        "name": "",
        "x": 700,
        "y": 120,
        "wires": [
            [
                "61764533.cf8b8c"
            ],
            [
                "61764533.cf8b8c"
            ]
        ]
    },
    {
        "id": "61764533.cf8b8c",
        "type": "debug",
        "z": "5c425f7e.794ee",
        "name": "Console Out",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "payload",
        "targetType": "msg",
        "x": 920,
        "y": 120,
        "wires": []
    },
    {
        "id": "40cfb9bc.9c0d98",
        "type": "inject",
        "z": "5c425f7e.794ee",
        "name": "Success",
        "topic": "",
        "payload": "test@test.test",
        "payloadType": "str",
        "repeat": "",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "x": 480,
        "y": 100,
        "wires": [
            [
                "a163646e.aaf7d8"
            ]
        ]
    },
    {
        "id": "27c7592.11bbe26",
        "type": "comment",
        "z": "5c425f7e.794ee",
        "name": "Sends Valid Email",
        "info": "",
        "x": 290,
        "y": 100,
        "wires": []
    },
    {
        "id": "ebd5e555.00ab28",
        "type": "comment",
        "z": "5c425f7e.794ee",
        "name": "Sends Timestamp",
        "info": "",
        "x": 290,
        "y": 140,
        "wires": []
    }
]
```

## Dependencies
- vanilla.js, https://validatejs.org

## License
MIT